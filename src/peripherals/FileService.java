package peripherals;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;

public class FileService {
    public FileService(){

    }


    public File chooseAFile(java.awt.Component mainWindowComponent) throws IOException, NullPointerException {
        File inputFileSpecified = null;
        String fileType;
        final JFileChooser fc = new JFileChooser("/home/pawel13/Documents/WireWorldInOut/");
        int returnVal = fc.showOpenDialog(mainWindowComponent);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            inputFileSpecified = fc.getSelectedFile();
            fileType = Files.probeContentType(inputFileSpecified.toPath());
            if (!fileType.equals("txt") && !fileType.equals("text/plain")) throw new IOException();
            System.out.println("File opened successfully");

        } else {
            System.err.println("File can not be opened");
            throw new NullPointerException();
        }

        return inputFileSpecified;
    }


    public String readFromFile(File inputFilename) throws IOException, NullPointerException {
        BufferedReader inputFileReader = new BufferedReader(new FileReader(inputFilename));
        StringBuilder strBld = new StringBuilder();
        String line;
        while ((line = inputFileReader.readLine()) != null)
            strBld.append(line).append("\n");

        inputFileReader.close();
        return strBld.toString();

    }

    public void writeToFile(File outputFilename, String text) throws IOException, NullPointerException {
        BufferedWriter outputFileWriter = new BufferedWriter(new FileWriter(outputFilename, false)); // true for appending, leave blank for default false
        outputFileWriter.write(text);
        outputFileWriter.close();
    }


}
