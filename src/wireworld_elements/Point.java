package wireworld_elements;

import wireworld_logic.State;

import java.util.Objects;

public class Point extends Element {
    private State state;

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Point(int x, int y, State state) {
        super(Direction.POINT);
        this.state = state;
        setxCoordinate(x);
        setyCoordinate(y);
        elementType = ElementType.POINT;

    }

    public Point(Point another) {
        super(Direction.POINT);
        this.state = another.state;
        this.setxCoordinate(another.getxCoordinate());
        this.setyCoordinate(another.getyCoordinate());
        this.elementType = ElementType.POINT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        if (!super.equals(o)) return false;
        Point point = (Point) o;
        //return (state == point.state && getxCoordinate() == point.getxCoordinate() && getyCoordinate() == point.getyCoordinate());
        return (getxCoordinate() == point.getxCoordinate() && getyCoordinate() == point.getyCoordinate());
    }

    @Override
    public int hashCode() {
        return 31 * getxCoordinate() + 23 * getyCoordinate(); // + state.hashCode();
        // return Objects.hash(super.hashCode(), state, getxCoordinate(), getyCoordinate());
    }

    @Override
    public String toString() {
        return state + " " + getxCoordinate() + "," + getyCoordinate() + "\n";
    }
}
