package wireworld_elements;

public enum ElementType {
    POINT, DIODE, GATE_AND, GATE_OR, GATE_XOR
}
