package wireworld_elements;

public class GateXor extends Element {

    public GateXor(int x, int y, Direction direction) {
        super(direction);
        setxCoordinate(x);
        setyCoordinate(y);
        elementType = ElementType.GATE_XOR;

    }
}

