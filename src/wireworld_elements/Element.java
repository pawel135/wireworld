package wireworld_elements;

import exceptions.ImproperElementRepresentationException;
import wireworld_logic.State;

import java.util.HashSet;
import java.util.Set;

public class Element implements Placeable {

    private int xCoordinate;
    private int yCoordinate;
    private Direction direction;
    private static int numberOfElements = 0;
    protected ElementType elementType;

    public HashSet<Point> getPointsCoveredByElement() {
        return pointsCoveredByElement;
    }

    protected HashSet<Point> pointsCoveredByElement = new HashSet<>();

    public Element(Direction direction) {
        this.direction = direction;
        numberOfElements++;
    }


    //@Override
    public boolean overlapsWithPointsSet(Set<Point> set) {
        return false;
    }

    //@Override
    public boolean isWithinImageLimits(Set<Point> set) {
        return true;
    }


    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setxCoordinate(int xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public void setyCoordinate(int yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public ElementType getElementType() {
        return elementType;
    }

    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Element)) return false;
        Element element = (Element) o;
        return xCoordinate == element.xCoordinate &&
                yCoordinate == element.yCoordinate && elementType == element.elementType;
    }

    @Override
    public int hashCode() {
        return 31 * xCoordinate + 17 * yCoordinate + 12 * elementType.hashCode();
        //    return Objects.hash(xCoordinate, yCoordinate, elementType); // Java 7+
    }


    @Override
    public String toString() {
        return elementType + " " + xCoordinate + "," + yCoordinate + " " + direction + "\n";
    }

    public void convertVisualInterpretationIntoElement(String visualInterpretation, int x, int y) throws ImproperElementRepresentationException {
        int i = 0, j = 0;
        String[] rows = visualInterpretation.split(";");
        for (String row : rows) {
            for (char c : row.toCharArray()) {
                switch (c) {
                    case '+':
                        pointsCoveredByElement.add(new wireworld_elements.Point(x + i, y + j, State.CONDUCTOR));
                        break;
                    case '.':
                        pointsCoveredByElement.add(new wireworld_elements.Point(x + i, y + j, State.EMPTY));
                        break;
                    default:
                        throw new ImproperElementRepresentationException();
                }
                i++;
            }
            i = 0;
            j++;
        }

    }
}
