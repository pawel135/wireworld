package wireworld_elements;

import exceptions.ImproperElementRepresentationException;

public class GateOr extends Element {

    @Override
    public String toString() {
        return "GateOr{" +
                "direction=" + getDirection() +
                ", x=" + getxCoordinate() +
                ", y=" + getyCoordinate() +
                "}";
    }

    public GateOr(int x, int y, Direction direction) {
        super(direction);
        setxCoordinate(x);
        setyCoordinate(y);
        elementType = ElementType.GATE_OR;


        // dimensions 5x4
        /**
         * GateOr: dimensions 5x4
         * Arrow point to the x,y element (coordinates given in the constructor)
         * Pixel scheme for Direction.RIGHT:
         a
         * x,y->    . . + .
         *          . + + +  -> aORb
         *          . . + .
         *              b
         * signal a at the top, signal b on the bottom, signal a OR b to the right
         *
         * Pixel scheme for Direction.LEFT:
         *            a
         * x,y ->   . + . .
         * aORb <-  + + + .
         *          . + . .
         *            b
         *
         */
        if (direction == Direction.LEFT) {
            try {
                convertVisualInterpretationIntoElement(
                        ".+..;" +
                                "+++.;" +
                                ".+..;", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }


        } else if (direction == Direction.UP) {
            try {
                convertVisualInterpretationIntoElement(
                        ".+.;" +
                                "+++;" +
                                ".+.;" +
                                "...", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        } else if (direction == Direction.DOWN) {
            try {
                convertVisualInterpretationIntoElement(
                        "..+..;" +
                                "...;" +
                                ".+.;" +
                                "+++;" +
                                ".+.", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        } else { //as for Direction.RIGHT which is the default
            try {
                convertVisualInterpretationIntoElement(
                        "..+.;" +
                                ".+++;" +
                                "..+.;", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        }
    }
}

