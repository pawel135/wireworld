package wireworld_elements;

import java.util.Set;

public interface Placeable {
    boolean overlapsWithPointsSet(Set<Point> set);

    boolean isWithinImageLimits(Set<Point> set);

}
