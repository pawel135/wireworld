package wireworld_elements;

public class GateAnd extends Element {

    public GateAnd(int x, int y, Direction direction) {
        super(direction);
        setxCoordinate(x);
        setyCoordinate(y);
        elementType = ElementType.GATE_AND;
    }

}
