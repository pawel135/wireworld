package wireworld_elements;

import exceptions.ImproperElementRepresentationException;

public class Diode extends Element {

    @Override
    public String toString() {
        return "Diode{" +
                "direction=" + getDirection() +
                ", x=" + getxCoordinate() +
                ", y=" + getyCoordinate() +
                "}";
    }

    public Diode(int x, int y, Direction direction) {
        super(direction);
        setxCoordinate(x);
        setyCoordinate(y);
        elementType = ElementType.DIODE;
        // dimensions 5x4
        /**
         * Diode: dimensions 5x4
         * Arrow point to the x,y element (coordinates given in the constructor)
         * Pixel scheme for Direction.RIGHT:
         * x,y->. . . .
         *      . + + .
         *      + + . +
         *      . + + .
         *      . . . .
         *
         * Pixel scheme for Direction.LEFT:
         *
         * x,y->. . . .
         *      . + + .
         *      + . + +
         *      . + + .
         *      . . . .
         */
        if (direction == Direction.LEFT) {
            try {
                convertVisualInterpretationIntoElement(
                        "....;" +
                                ".++.;" +
                                "+.++;" +
                                ".++.;" +
                                "....", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }


        } else if (direction == Direction.UP) {
            try {
                convertVisualInterpretationIntoElement(
                        "..+..;" +
                                ".+.+.;" +
                                ".+++.;" +
                                "..+..", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        } else if (direction == Direction.DOWN) {
            try {
                convertVisualInterpretationIntoElement(
                        "..+..;" +
                                ".+++.;" +
                                ".+.+.;" +
                                "..+..", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        } else { //as for Direction.RIGHT which is the default
            try {
                convertVisualInterpretationIntoElement(
                        "....;" +
                                ".++.;" +
                                "++.+;" +
                                ".++.;" +
                                "....", x, y);

            } catch (ImproperElementRepresentationException imElRepEx) {
                System.err.println(elementType + " cannot be created using a conversion from Visual Representation because of improper formatting");
            }
        }
    }


}


