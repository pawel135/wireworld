package wireworld_elements;

public enum Direction {
    RIGHT, LEFT, UP, DOWN, POINT // POINT for 1 pixel Elements
}
