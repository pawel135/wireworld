package wireworld_logic;

import exceptions.BadFileFormattingException;
import wireworld_elements.*;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;

import static java.lang.Thread.sleep;


public class PictureMaker {
    private BufferedImage bufferedImage;
    private HashSet<wireworld_elements.Point> currentGeneration; // = new HashSet<>(); // after converting eacg element to a set of points
    private ArrayList<Element> elements; // = new ArrayList<Element>();
    private Logic wireWorldLogic; // = new Logic();
    private final static double SCALE_WIDTH = 4.0;
    private final static double SCALE_HEIGHT = 4.0;
    private final static int IMAGE_MAX_WIDTH = (int) (100 * SCALE_WIDTH);
    private final static int IMAGE_MAX_HEIGHT = (int) (100 * SCALE_HEIGHT);
    private static int IMAGE_WIDTH;
    private static int IMAGE_HEIGHT;

    public static int getImageWidth() {
        return IMAGE_WIDTH;
    }

    public static int getImageHeight() {
        return IMAGE_HEIGHT;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public static double getScaleWidth() {
        return SCALE_WIDTH;
    }

    public static double getScaleHeight() {
        return SCALE_HEIGHT;
    }

    public PictureMaker() {
        currentGeneration = new HashSet<>(); // after converting eacg element to a set of points
        elements = new ArrayList<Element>();
        wireWorldLogic = new Logic();
       /* try {
            bufferedImage = ImageIO.read(new File("/home/pawel13/Documents/WireWorldInOut/Selection_006.png"));
        } catch (IOException ioEx) {
            System.err.println("Could not open the png file: " + ioEx.toString());
        }
        */

        makeInitialImage();
    }

    private void makeInitialImage() {
        bufferedImage = new BufferedImage(IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        int a = 255; // alpha (opacity) 0..255
        int r = 255; // red component 0..255
        int g = 0; // green component 0..255
        int b = 0; // blue component 0..255
        int colorRGB = (a << 24) | (r << 16) | (g << 8) | b;
        for (int i = 0; i < IMAGE_MAX_WIDTH; i++) {
            for (int j = 0; j < IMAGE_MAX_HEIGHT; j++) {
                bufferedImage.setRGB(i, j, colorRGB); //  for explicitly specified value
                bufferedImage.setRGB(i, j, Color.BLACK.getRGB());
            }
        }
    }

    public void makeFirstGeneration(String inputStr) throws NumberFormatException, BadFileFormattingException {
        /**
         Elements should not overlap. Only ELECTRON_HEAD should be placed on some conductor previously specified
         (including those being a part of Elements: DIODE etc.)

         Input file format:

         width height
         element_name xCoordinate,yCoordinate direction
         ...


         where: element_name from the enum ElementEnum excluding ELECTRON_TAIL

         example:

         300 300
         DIODE 5,10 RIGHT
         CONDUCTOR 20-30,40
         ELECTRON_HEAD 20,40
         **/


        String[] inputFileLines = inputStr.split("\n");
        String elementName;
        Direction elementDirection;
        int xCoordinate[] = new int[2];
        int yCoordinate[] = new int[2];

        IMAGE_WIDTH = Integer.parseInt(getArraySubelementOnIndex(inputFileLines[0].split(" "), 0)); // throws NumberFormatException
        IMAGE_HEIGHT = Integer.parseInt(getArraySubelementOnIndex(inputFileLines[0].split(" "), 1)); // throws NumberFormatException

        for (int i = 1; i < inputFileLines.length; i++) {
            if (inputFileLines[i].split(" ").length < 2)
                throw new BadFileFormattingException();

            elementName = getArraySubelementOnIndex(inputFileLines[i].split(" "), 0);

            //get element direction form 2nd place
            try {
                switch (getArraySubelementOnIndex(inputFileLines[i].split(" "), 2)) {
                    case "RIGHT":
                        elementDirection = Direction.RIGHT;
                        break;
                    case "LEFT":
                        elementDirection = Direction.LEFT;
                        break;
                    case "UP":
                        elementDirection = Direction.UP;
                        break;
                    case "DOWN":
                        elementDirection = Direction.DOWN;
                        break;
                    default:
                        elementDirection = Direction.RIGHT;
                        break;
                }
            } catch (ArrayIndexOutOfBoundsException e1) {
                elementDirection = Direction.RIGHT;
            }


            try {
                xCoordinate[0] = Integer.parseInt(getArraySubelementOnIndex(getArraySubelementOnIndex(
                        getArraySubelementOnIndex(inputFileLines[i].split(" "), 1).split(","), 0).split("-"), 0));// throws NumberFormatException
            } catch (ArrayIndexOutOfBoundsException arInExc) {
                throw new BadFileFormattingException();
            }

            try {
                if (elementName.equals("CONDUCTOR")) {
                    xCoordinate[1] = Integer.parseInt(getArraySubelementOnIndex(getArraySubelementOnIndex(
                            getArraySubelementOnIndex(inputFileLines[i].split(" "), 1).split(","), 0).split("-"), 1));
                } else {
                    xCoordinate[1] = xCoordinate[0];
                }
            } catch (ArrayIndexOutOfBoundsException arInExc) {
                xCoordinate[1] = xCoordinate[0];
            }

            try {
                yCoordinate[0] = Integer.parseInt(getArraySubelementOnIndex(getArraySubelementOnIndex(
                        getArraySubelementOnIndex(inputFileLines[i].split(" "), 1).split(","), 1).split("-"), 0));// throws NumberFormatException
            } catch (ArrayIndexOutOfBoundsException arInExc) {
                throw new BadFileFormattingException();
            }
            try {
                if (elementName.equals("CONDUCTOR")) {
                    yCoordinate[1] = Integer.parseInt(getArraySubelementOnIndex(getArraySubelementOnIndex(
                            getArraySubelementOnIndex(inputFileLines[i].split(" "), 1).split(","), 1).split("-"), 1));
                } else {
                    yCoordinate[1] = yCoordinate[0];
                }
            } catch (ArrayIndexOutOfBoundsException arInExc) {
                yCoordinate[1] = yCoordinate[0];
            }

            xCoordinate[0] = Math.min(xCoordinate[0], xCoordinate[1]);
            xCoordinate[1] = Math.max(xCoordinate[0], xCoordinate[1]);

            yCoordinate[0] = Math.min(yCoordinate[0], yCoordinate[1]);
            yCoordinate[1] = Math.max(yCoordinate[0], yCoordinate[1]);


            addElementToArrayList(elementName, xCoordinate, yCoordinate, elementDirection);

        }

        convertElementCollectionToPointCollection();

        convertPointCollectionToImage();

    }


    private void convertPointCollectionToImage() {
        bufferedImage = new BufferedImage(IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT, BufferedImage.TYPE_INT_ARGB);
        for (int i = 0; i < IMAGE_MAX_WIDTH; i++) {
            for (int j = 0; j < IMAGE_MAX_HEIGHT; j++) {
                bufferedImage.setRGB(i, j, Color.BLACK.getRGB());
            }
        }
        for (wireworld_elements.Point point : currentGeneration) {
            bufferedImage.setRGB(point.getxCoordinate(), point.getyCoordinate(), point.getState().getColor().getRGB());


        }
    }



    public void makeNewGeneration() {
        currentGeneration = wireWorldLogic.createNewGeneration(currentGeneration);
        convertPointCollectionToImage();
    }

    public <T> T getArraySubelementOnIndex(T[] array, int n) throws ArrayIndexOutOfBoundsException {
        return array[n];
    }


    private void addElementToArrayList(String elementName, int xCoordinate[], int yCoordinate[], Direction elementDirection) {
        switch (elementName) {
            case "CONDUCTOR":
                for (int j = xCoordinate[0]; j <= xCoordinate[1]; j++) {
                    for (int k = yCoordinate[0]; k <= yCoordinate[1]; k++) {
                        elements.add(new wireworld_elements.Point(j, k, State.CONDUCTOR));
                    }
                }
                break;
            case "ELECTRON_HEAD":
                for (int j = xCoordinate[0]; j <= xCoordinate[1]; j++) {
                    for (int k = yCoordinate[0]; k <= yCoordinate[1]; k++) {
                        elements.add(new wireworld_elements.Point(j, k, State.ELECTRON_HEAD));
                    }
                }
                break;
            case "EMPTY":
                for (int j = xCoordinate[0]; j <= xCoordinate[1]; j++) {
                    for (int k = yCoordinate[0]; k <= yCoordinate[1]; k++) {
                        elements.add(new wireworld_elements.Point(j, k, State.EMPTY));
                    }
                }
                break;

            case "ELECTRON_TAIL":
                for (int j = xCoordinate[0]; j <= xCoordinate[1]; j++) {
                    for (int k = yCoordinate[0]; k <= yCoordinate[1]; k++) {
                        elements.add(new wireworld_elements.Point(j, k, State.ELECTRON_TAIL));
                    }
                }
                break;
            case "DIODE":
                elements.add(new Diode(xCoordinate[0], yCoordinate[0], elementDirection));

                break;
            case "GATE_AND":
                elements.add(new GateAnd(xCoordinate[0], yCoordinate[0], elementDirection));
                break;
            case "GATE_OR":
                elements.add(new GateOr(xCoordinate[0], yCoordinate[0], elementDirection));
                break;
            case "GATE_XOR":
                elements.add(new GateXor(xCoordinate[0], yCoordinate[0], elementDirection));
                break;

            default:
                break;
        }
    }


    private void convertElementCollectionToPointCollection() {
        Iterator<Element> elementIterator = elements.iterator();

        while (elementIterator.hasNext()) {
            Element element = elementIterator.next();
            if (element.overlapsWithPointsSet(currentGeneration)) {
                // TODO: ignore the element or show an error?

            } else {
                if (element instanceof wireworld_elements.Point) {
                    currentGeneration.add((wireworld_elements.Point) element);
                } else {
                    for (wireworld_elements.Point point : element.getPointsCoveredByElement()) {
                        currentGeneration.add(point);
                    }
                }
            }
        }

    }

    public String convertGenerationToText() {
        StringBuilder generationAsText = new StringBuilder();
        Iterator<wireworld_elements.Point> pointIterator = currentGeneration.iterator();

        generationAsText.append(getImageWidth() + " " + getImageHeight() + "\n");

        while (pointIterator.hasNext()) {
            wireworld_elements.Point point = pointIterator.next();
            generationAsText.append(point.toString());

        }
        return generationAsText.toString();

    }
}

