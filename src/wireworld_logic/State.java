package wireworld_logic;

import java.awt.*;

public enum State {
    EMPTY(Color.black), CONDUCTOR(Color.yellow), ELECTRON_HEAD(Color.blue), ELECTRON_TAIL(Color.red);
    private final Color color;

    State(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
