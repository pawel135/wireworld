package wireworld_logic;

import wireworld_elements.Element;
import wireworld_elements.Point;

import java.util.HashSet;
import java.util.Iterator;

public class Logic {

    public HashSet<wireworld_elements.Point> createNewGeneration(HashSet<wireworld_elements.Point> currentGeneration) {
        HashSet<wireworld_elements.Point> nextGeneration = new HashSet<>();
        wireworld_elements.Point temp;
        int numberOfAdjacentHeads = 0;
        for (wireworld_elements.Point point : currentGeneration) {

            temp = new Point(point);

            switch (point.getState()) {
                case EMPTY:
                    nextGeneration.add(point);
                    break;

                case ELECTRON_HEAD:
                    temp.setState(State.ELECTRON_TAIL);
                    nextGeneration.add(temp);
                    break;

                case ELECTRON_TAIL:
                    temp.setState(State.CONDUCTOR);
                    nextGeneration.add(temp);
                    break;
                default:
                    numberOfAdjacentHeads = numberOfAdjacentHeads(currentGeneration, point.getxCoordinate(), point.getyCoordinate());
                    if (numberOfAdjacentHeads == 1 || numberOfAdjacentHeads == 2) {
                        temp.setState(State.ELECTRON_HEAD);
                    } else {
                        temp.setState(State.CONDUCTOR);
                    }
                    nextGeneration.add(temp);
                    break;
            }
        }

        return nextGeneration;
    }

    public int numberOfAdjacentHeads(HashSet<wireworld_elements.Point> currentGeneration, int x, int y) {
        int adjacentHeads = 0;
        Iterator<Point> pointIterator = currentGeneration.iterator();
        while (pointIterator.hasNext()) {
            wireworld_elements.Point point = pointIterator.next();

            if ((java.lang.Math.abs(point.getxCoordinate() - x) <= 1) && (java.lang.Math.abs(point.getyCoordinate() - y) <= 1) && (point.getState() == State.ELECTRON_HEAD)
                    && (point.getxCoordinate() != x || point.getyCoordinate() != y)) {
                adjacentHeads++;

            }
        }


        return adjacentHeads;
    }


}
