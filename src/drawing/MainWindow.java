package drawing;

import exceptions.BadFileFormattingException;
import wireworld_logic.PictureMaker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;
import java.io.IOException;

public class MainWindow extends JFrame {
    private JButton closeButton;
    private JPanel mainPanel;
    private JButton outputFileButton;
    private JLabel inputFileStatus;
    private JLabel outputFileStatus;
    private JPanel pngPanel;
    private JButton inputFileButton;
    private JButton startButton;
    private JButton stopButton;
    private JTextField numberOfGenerationsTextField;
    private JLabel numberOfGenerationsLabel;
    private JButton resetButton;
    private File inputFileSpecified;
    private File outputFileSpecified;
    private String inputFileText = "";
    private String outputFileText = "";
    private peripherals.FileService fileSvc = new peripherals.FileService();
    private PictureMaker pictureMaker;
    private static int numberOfGenerations;
    private static int countOfGenerations;
    private Timer generationAnimationTimer;

    public MainWindow() {
        super("Fire World");
        initComponents();
        setVisible(true);

        pngPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                ((PictureArea) pngPanel).paintComponent(getGraphics());
            }
        });
    }


    public void initComponents() {
        setContentPane(mainPanel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocation(super.getX() + super.getWidth() / 2, super.getY());
        //setResizable(false);
        Dimension screenDimension = getToolkit().getScreenSize();
        Dimension windowDimension = new Dimension((int) (screenDimension.getWidth() / 1), (int) (screenDimension.getHeight() / 1));
        setPreferredSize(windowDimension);
        setAutoRequestFocus(true);
        // setMinimumSize(new Dimension(1000, 700));
        setMaximumSize(screenDimension);
        //pngPanel.setPreferredSize(new Dimension((int) (screenDimension.getWidth()/1.2 -100), (int) (screenDimension.getHeight()/1.2) - 50));
        countOfGenerations = 0;


        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                startButton.setEnabled(false);
                try {
                    inputFileText = fileSvc.readFromFile(inputFileSpecified);

                } catch (Exception ex1) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "You have to specify the input file properly.",
                            "No input file",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;
                }


                if (outputFileSpecified == null) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "You have to specify the output file properly.",
                            "No output file",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;
                }


                try {
                    numberOfGenerations = Integer.valueOf(numberOfGenerationsTextField.getText());
                    if (numberOfGenerations < 0) throw new NumberFormatException();
                } catch (NumberFormatException nFEx) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "You have to specify the number of generations properly. It should be an integer non-negative number",
                            "Improper number of generations",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;
                }



                try {
                    pictureMaker.makeFirstGeneration(inputFileText); // throws NumberFormatException
                    ((PictureArea) pngPanel).paintComponent(getGraphics());
                } catch (NumberFormatException numbForExc) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "Improper input file formatting! Should be: \n" +
                                    "[WIDTH] [HEIGHT] \n" +
                                    "[ELEMENT_TYPE] [X_COORDINATE | X_RANGE],[Y_COORDINATE | Y_RANGE] \n " +
                                    "Where range is specified by two numbers separated by \"-\" ",
                            "Wrong file format",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;

                } catch (BadFileFormattingException badFormatExc) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "Improper input file formatting! Should be: \n" +
                                    "[WIDTH] [HEIGHT] \n" +
                                    "[ELEMENT_TYPE] [X_COORDINATE | X_RANGE],[Y_COORDINATE | Y_RANGE] \n " +
                                    "Where range is specified by two numbers separated by \"-\" ",
                            "Too few arguments",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;
                }



                countOfGenerations = 0; // only if should start from the beginning each time
                int speed = 100;
                int pause = 1000;
                generationAnimationTimer = new Timer(speed, new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {

                        if (countOfGenerations >= numberOfGenerations) {
                            ((Timer) actionEvent.getSource()).stop();
                            startButton.setEnabled(true);
                            return;
                        }
                        makeMultipleGenerations();
                        countOfGenerations++;
                    }
                });

                generationAnimationTimer.setInitialDelay(pause);
                generationAnimationTimer.start();

                outputFileText = pictureMaker.convertGenerationToText();

                try {
                    fileSvc.writeToFile(outputFileSpecified, outputFileText);

                } catch (Exception ex2) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "You have to specify output file properly.",
                            "No output file",
                            JOptionPane.WARNING_MESSAGE);
                    startButton.setEnabled(true);
                    return;
                }

            }


        });


        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    if (!generationAnimationTimer.isRunning()) {
                        JOptionPane.showMessageDialog(MainWindow.this, "You cannot stop as no animation is currently running.", "Cannot stop", JOptionPane.WARNING_MESSAGE);
                    } else {
                        generationAnimationTimer.stop();
                        startButton.setEnabled(true);
                    }
                } catch (NullPointerException nPEx) {
                    JOptionPane.showMessageDialog(MainWindow.this, "You cannot stop as no animation has been started.", "Cannot stop", JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                createUIComponents();
                ((PictureArea) pngPanel).paintComponent(getGraphics());

            }
        });

        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                int dialogButton = JOptionPane.YES_NO_CANCEL_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(MainWindow.this, "Do you want to save the current generation before closing?", "Confirmation", dialogButton);

                if (dialogResult == JOptionPane.NO_OPTION) {
                    if (inputFileSpecified == null)
                        System.exit(0);
                    try {
                        fileSvc.writeToFile(outputFileSpecified, inputFileText);
                    } catch (Exception nEx1) {
                        JOptionPane.showMessageDialog(MainWindow.this,
                                "You have to specify files properly.",
                                "No file",
                                JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    System.exit(0);
                }

                if (dialogResult == JOptionPane.YES_OPTION) {
                    MainWindow.super.requestFocus(true);

                    try {
                        fileSvc.writeToFile(outputFileSpecified, pictureMaker.convertGenerationToText());
                    } catch (Exception nEx1) {
                        JOptionPane.showMessageDialog(MainWindow.this,
                                "You have to specify files properly.",
                                "No file",
                                JOptionPane.WARNING_MESSAGE);
                        return;
                    }
                    System.exit(0);
                }
            }
        });

        inputFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    inputFileSpecified = fileSvc.chooseAFile(MainWindow.this);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "Wrong input file type. Should be txt or text/plain.",
                            "Problem with an input file",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                } catch (NullPointerException nEx) {
                    return;
                }
                inputFileStatus.setText(inputFileSpecified.toString());
            }
        });


        outputFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    outputFileSpecified = fileSvc.chooseAFile(MainWindow.this);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(MainWindow.this,
                            "Wrong output file type. Should be txt or text/plain.",
                            "Problem with an output file",
                            JOptionPane.WARNING_MESSAGE);
                    return;
                } catch (NullPointerException nEx) {
                    return;
                }
                outputFileStatus.setText(outputFileSpecified.toString());
            }
        });
    }


    public void createUIComponents() {
        pngPanel = new PictureArea(0, 0, 200, 200);
        pictureMaker = ((PictureArea) pngPanel).getPictureMaker();
    }

    public void makeMultipleGenerations() /*throws NumberFormatException, BadFileFormattingException*/ {
        /* these two lines make start button always begin from the beginning */
        //pngPanel = new PictureArea(0, 0, 1000, 1000, numberOfGenerations);
        //pictureMaker = ((PictureArea) pngPanel).getPictureMaker();

        pictureMaker.makeNewGeneration();
        ((PictureArea) pngPanel).paintComponent(getGraphics());

    }
}