package drawing;

import wireworld_logic.PictureMaker;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class PictureArea extends JPanel {
    private BufferedImage bufferedImage;
    private int locationX, locationY, width, height;

    public PictureMaker getPictureMaker() {
        return pictureMaker;
    }

    private PictureMaker pictureMaker;

    public PictureArea() {

    }

    public PictureArea(int x, int y, int width, int height) {
        //locationX = x;
        //locationY = y;
        //this.width = width;
        //this.height = height;
        //setLayout(null);
        //setLocation(x, y);
        //setSize(width, height);
        //setPreferredSize(new Dimension(width,height));
        pictureMaker = new PictureMaker();

    }


    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        bufferedImage = pictureMaker.getBufferedImage();

        //width = PictureMaker.getImageWidth();
        //height = PictureMaker.getImageHeight();

        //g.setColor(Color.BLUE);
        //g.drawLine(10, 10, 200, 200);

        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage after = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        AffineTransform at = new AffineTransform();
        at.scale(PictureMaker.getScaleWidth(), PictureMaker.getScaleHeight());
        AffineTransformOp scaleOp =
                new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
        after = scaleOp.filter(bufferedImage, after);


        g.drawImage(after, 0, 0, null);
        setBounds(locationX, locationY, java.lang.Math.min(width, getWidth()), height); // (width > getWidth())?getWidth():width


    }


}
