package test;

import exceptions.ImproperElementRepresentationException;
import wireworld_elements.Direction;
import wireworld_logic.State;

import static org.junit.Assert.*;

public class ElementTest {

    @org.junit.Test
    public void convertVisualInterpretationIntoElementTest() throws ImproperElementRepresentationException {
        int x = 0;
        int y = 0;
        String visualInterpretation =
                "....;" +
                        ".++.;" +
                        "+.++;" +
                        ".++.;" +
                        "....";
        wireworld_elements.Element element = new wireworld_elements.Element(Direction.RIGHT);
        try {
            element.convertVisualInterpretationIntoElement(visualInterpretation, x, y);

        } catch (ImproperElementRepresentationException e) {
            fail("Improper string formatting");
        }

        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x, y, State.EMPTY))) fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 3, y, State.EMPTY))) fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 1, y + 1, State.EMPTY)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x, y + 2, State.EMPTY))) fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 3, y + 2, State.EMPTY)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x, y + 3, State.EMPTY))) fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 1, y + 3, State.EMPTY)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 2, y + 3, State.EMPTY)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 3, y + 3, State.EMPTY)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 1, y, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 2, y, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x, y + 1, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 2, y + 1, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 3, y + 1, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 1, y + 2, State.CONDUCTOR)))
            fail();
        if (!element.getPointsCoveredByElement().contains(new wireworld_elements.Point(x + 2, y + 2, State.CONDUCTOR)))
            fail();

    }

}
