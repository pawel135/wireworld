package test;

import org.junit.Test;
import wireworld_elements.Point;
import wireworld_logic.Logic;
import wireworld_logic.State;

import java.util.HashSet;

import static org.junit.Assert.*;


public class LogicTest {

    @Test
    public void numberOfAdjacentHeadsTest() {
        HashSet<wireworld_elements.Point> currentGeneration = new HashSet<>();
        Logic logic = new Logic();
        currentGeneration.add(new wireworld_elements.Point(0, 0, State.ELECTRON_HEAD));
        currentGeneration.add(new wireworld_elements.Point(0, 1, State.EMPTY));
        currentGeneration.add(new wireworld_elements.Point(0, 2, State.EMPTY));
        currentGeneration.add(new wireworld_elements.Point(1, 0, State.ELECTRON_HEAD));
        currentGeneration.add(new wireworld_elements.Point(1, 1, State.EMPTY));
        currentGeneration.add(new wireworld_elements.Point(1, 2, State.CONDUCTOR));
        currentGeneration.add(new wireworld_elements.Point(2, 0, State.ELECTRON_HEAD));
        currentGeneration.add(new wireworld_elements.Point(2, 1, State.CONDUCTOR));
        currentGeneration.add(new wireworld_elements.Point(2, 2, State.ELECTRON_HEAD));

        assertEquals("Wrong implementation of adjacent heads counter", 1, logic.numberOfAdjacentHeads(currentGeneration, 0, 0));
        assertEquals("Wrong implementation of adjacent heads counter", 2, logic.numberOfAdjacentHeads(currentGeneration, 0, 1));
        assertEquals("Wrong implementation of adjacent heads counter", 0, logic.numberOfAdjacentHeads(currentGeneration, 0, 2));
        assertEquals("Wrong implementation of adjacent heads counter", 2, logic.numberOfAdjacentHeads(currentGeneration, 1, 0));
        assertEquals("Wrong implementation of adjacent heads counter", 4, logic.numberOfAdjacentHeads(currentGeneration, 1, 1));
        assertEquals("Wrong implementation of adjacent heads counter", 1, logic.numberOfAdjacentHeads(currentGeneration, 1, 2));
        assertEquals("Wrong implementation of adjacent heads counter", 1, logic.numberOfAdjacentHeads(currentGeneration, 2, 0));
        assertEquals("Wrong implementation of adjacent heads counter", 3, logic.numberOfAdjacentHeads(currentGeneration, 2, 1));
        assertEquals("Wrong implementation of adjacent heads counter", 0, logic.numberOfAdjacentHeads(currentGeneration, 2, 2));

    }


    //public int numberOfAdjacentHeads(HashSet<Point> currentGeneration, int x, int y)


}