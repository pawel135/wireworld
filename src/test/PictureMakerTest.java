package test;

import org.junit.Test;
import wireworld_logic.PictureMaker;
import static org.junit.Assert.*;

public class PictureMakerTest {

    @Test
    public void getArraySubelementOnIndexTest() {
        // Character [] letters = new Character[]{'a','b','c','d'};
        // Character [] letters = {'a','b','c','d'};
        assertEquals("Bad value of Array subelement.", Character.valueOf('c'),
                new PictureMaker().getArraySubelementOnIndex((new Character[]{'a', 'b', 'c', 'd'}), 2));

    }

}