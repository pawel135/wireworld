package test;

import org.junit.Test;
import wireworld_logic.State;

import static org.junit.Assert.*;

public class PointTest {

    @Test
    public void equalsTest() {
        wireworld_elements.Point p1 = new wireworld_elements.Point(0, 2, State.CONDUCTOR);
        wireworld_elements.Point p2 = new wireworld_elements.Point(0, 2, State.ELECTRON_HEAD);

        if (!p1.equals(p2)) fail();

    }

    @Test
    public void pointCopyConstructorTest() {
        wireworld_elements.Point p1 = new wireworld_elements.Point(0, 2, State.CONDUCTOR);
        wireworld_elements.Point p2 = new wireworld_elements.Point(p1);

        if (!(p1.getState().equals(p2.getState()))) fail("Different states");
        if (p1.getxCoordinate() != p2.getxCoordinate()) fail("Different x coordinates");
        if (p1.getyCoordinate() != p2.getyCoordinate()) fail("Different y coordinates");
        if (!(p1.getDirection().equals(p2.getDirection()))) fail("Different directions");
        if (!(p1.getElementType().equals(p2.getElementType()))) fail("Different element types");
    }

}