package test;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import static org.junit.Assert.*;

public class FileServiceTest {


    @org.junit.Test
    public void testReadFromFile() throws Exception {
        String filename = "testFilename.txt";
        PrintWriter writer = new PrintWriter(filename, "UTF-8");
        writer.println("First line");
        writer.println("Second line");
        writer.close();
        File file = new File(filename);
        String expectedString = "First line\nSecond line\n";
        assertEquals("Not matching strings: expected and actual saved with the tested function.", expectedString, (new peripherals.FileService()).readFromFile(file));

        try {
            new peripherals.FileService().readFromFile(null);
            fail("Null pointer expected but not thrown");
        } catch (NullPointerException nPEx) {

        }

    }


    @org.junit.Test
    public void testWriteToFile() {
        try {
            (new peripherals.FileService()).writeToFile(null, "");
            fail("Exception expected but not thrown");
        } catch (NullPointerException nEx1) {
            System.out.println(nEx1.getMessage());
        } catch (IOException ex1) {
            System.out.println(ex1.getMessage());
        }


    }

}